import * as signalR from "@microsoft/signalr";
import { Sensor } from "../model/sensor";
import { debounceTime } from "rxjs";
import { createProperty } from "../hooks/createProperty";
import { sensorRepository } from "../repositories/sensorRepository";

type HubState = "connecting" | "connected" | "disconnected";
const hubState = createProperty<HubState>("connecting");

hubState.observable.pipe(debounceTime(1000)).subscribe((state) => {
  if (state === "disconnected")
    console.warn("Die Serververbindung wurde unterbrochen.");
  else if (state === "connecting") console.info("Verbinde sensorHub...");
  else if (state === "connected") console.info("SensorHub Verbunden.");
});

const sensorHubConnection = new signalR.HubConnectionBuilder()
  .withUrl("hubs/sensorHub")
  .withAutomaticReconnect()
  .build();

sensorHubConnection.onreconnected((cb) => {
  hubState.set("connected");
  console.info("SignalR reconnected:", cb);
});

sensorHubConnection.onreconnecting((cb) => {
  if (hubState.value() !== "connecting") {
    hubState.set("connecting");
    console.info("SignalR reconnecting:", cb);
  }
});

sensorHubConnection.on("initialize", (sensors: Sensor[]) => {
  console.debug(`Receiving ${sensors.length} initial sensors`, sensors);
  sensorRepository.replace(sensors);
});

sensorHubConnection.on("updateSensors", (sensors: Sensor[]) => {
  sensors.forEach((sensor) => {
    sensorRepository.add(sensor);
  });
});

/**
 * Connects to the signalr-hub for events.
 */
export const startSensorHub = () => {
  if (sensorHubConnection.state === "Connected") return;

  console.debug("Starting sensorHub");

  if (sensorHubConnection.state !== signalR.HubConnectionState.Disconnected) {
    console.warn("SensorHub is not disconnected, trying to disconnet");
    sensorHubConnection.stop();
  }

  sensorHubConnection
    .start()
    .then(() => {
      console.info("Connected to sensorHub");
      hubState.set("connected");
    })
    .catch((err) => {
      console.error("Cant connect to sensorHub", err);
      hubState.set("disconnected");
    });
};

/**
 * Disconnects from the signalr-hub.
 */
export const stopEventHub = () => {
  console.debug("Stopping event hub");
  sensorHubConnection.stop();
  hubState.set("disconnected");
};

/**
 * Gets the current hub connection state
 */
export const useHubState = hubState.useState;
