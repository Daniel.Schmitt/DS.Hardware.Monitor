import { Sensor } from "../model/sensor";

export const getLastSensorsOfName = (
  sensors: Array<Sensor>,
  name: string
): Array<Sensor> => {
  return sensors
    .toList()
    .Where((x) => x!.name == name)
    .OrderBy((x) => x.dateTime)
    .ToArray();
};

export const takeLastSensorsOfName = (
  take: number,
  sensors: Array<Sensor>,
  name: string
): Array<Sensor> => {
  return sensors
    .toList()
    .Where((x) => x!.name == name)
    .OrderByDescending((x) => x.dateTime)
    .Take(take)
    .Reverse()
    .ToArray();
};

export const getLastSensors = (sensors: Array<Sensor>): Array<Sensor> => {
  return sensors
    .toList()
    .OrderByDescending((x) => x.dateTime)
    .DistinctBy((x) => x.name)
    .Reverse()
    .ToArray();
};
