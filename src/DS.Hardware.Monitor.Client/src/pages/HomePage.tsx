import { Box, Grid } from "@mui/material";
import { useSensors } from "../repositories/sensorRepository";
import { startSensorHub } from "../hubs/sensorHub";
import {
  getLastSensors,
  takeLastSensorsOfName,
} from "../helpers/sensorsHelper";
import { SensorCardWithChart } from "../components/SensorCardWithChart";

startSensorHub();
export const HomePage = () => {
  const sensors = useSensors();
  const lastSensors = getLastSensors(sensors);

  return (
    <Box
      display="flex"
      justifyContent="center"
      alignItems="center"
      minHeight="100vh"
    >
      <Box m={2}>
        <Grid container justifyContent="center" spacing={2}>
          {lastSensors.map((sensor, index) => (
            <Grid item key={index}>
              {/* <SensorCard sensor={sensor} /> */}
              <SensorCardWithChart
                sensor={sensor}
                sensorsForChart={takeLastSensorsOfName(
                  50,
                  sensors,
                  sensor.name
                )}
              />
            </Grid>
          ))}
        </Grid>
      </Box>
    </Box>
  );
};
