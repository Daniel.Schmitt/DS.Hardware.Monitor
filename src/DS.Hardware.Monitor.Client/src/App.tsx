import { HomePage } from "./pages/HomePage";
import React, { Component } from "react";
import { Route, Routes } from "react-router-dom";
import { Layout } from "./Layout";
import {
  createTheme,
  CssBaseline,
  ThemeProvider,
  useMediaQuery,
} from "@mui/material";

export function App() {
  const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)");
  const theme = createTheme({
    palette: {
      mode: prefersDarkMode ? "dark" : "light",
      ...(prefersDarkMode
        ? {
            type: "dark",
            primary: {
              main: "#36b686",
            },
            secondary: {
              main: "#ffb000",
            },
            background: {
              default: "#121212",
              paper: "#303030",
            },
          }
        : {
            type: "light",
            primary: {
              main: "#36b686",
            },
            secondary: {
              main: "#ffb000",
            },
            background: {
              default: "#fafafa",
              paper: "#fff",
            },
          }),
    },
  });

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Layout>
        <Routes>
          <Route path="/" element={<HomePage />} />
        </Routes>
      </Layout>
    </ThemeProvider>
  );
}
