import {
  Card,
  CardContent,
  CardHeader,
  CardProps,
  Stack,
  Typography,
} from "@mui/material";
import { useScreenSize } from "../hooks/useScreenSize";
import { Sensor } from "../model/sensor";

export type SensorCardProps = CardProps & {
  sensor: Sensor;
};

export function SensorCard(props: SensorCardProps) {
  const screenSizeIsDesktop = useScreenSize().isDesktop();
  const { sensor, ...cardProps } = props;

  return (
    <Card {...cardProps} sx={{ minWidth: screenSizeIsDesktop ? 250 : 150 }}>
      <CardHeader
        title={`${sensor.name}`}
        titleTypographyProps={{
          variant: "body1",
        }}
      />
      <CardContent>
        <Stack>
          <Typography
            variant={screenSizeIsDesktop ? "h4" : "h5"}
            align="center"
          >
            {`${sensor.value.toFixed(2)}
                   ${sensor.unit}`}
          </Typography>
        </Stack>
      </CardContent>
    </Card>
  );
}
