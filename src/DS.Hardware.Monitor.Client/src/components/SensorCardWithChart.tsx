import { Card, CardProps, Stack, Typography } from "@mui/material";
import { Area, AreaChart, ResponsiveContainer } from "recharts";
import { takeLastSensorsOfName } from "../helpers/sensorsHelper";
import { useScreenSize } from "../hooks/useScreenSize";
import { Sensor } from "../model/sensor";

export type SensorCardWithChartProps = CardProps & {
  sensor: Sensor;
  sensorsForChart: Sensor[];
};

export function SensorCardWithChart(props: SensorCardWithChartProps) {
  const screenSizeIsDesktop = useScreenSize().isDesktop();
  const { sensor, sensorsForChart, ...cardProps } = props;

  return (
    <Card {...cardProps} sx={{ minWidth: screenSizeIsDesktop ? 250 : 150 }}>
      <Typography ml={1} mt={1}>{`${sensor.name}`}</Typography>
      <Stack mt={3} mb={-6}>
        <Typography
          zIndex={1}
          variant={screenSizeIsDesktop ? "h4" : "h5"}
          align="center"
        >
          {`${sensor.value.toFixed(2)}
                   ${sensor.unit}`}
        </Typography>
      </Stack>
      <ResponsiveContainer width={"100%"} height={80}>
        <AreaChart data={sensorsForChart} margin={{ right: 0, left: 0 }}>
          <Area
            type="monotone"
            dataKey="value"
            // todo theme colors
            stroke="#505050"
            fill="#404040"
            isAnimationActive={false}
          />
        </AreaChart>
      </ResponsiveContainer>
    </Card>
  );
}
