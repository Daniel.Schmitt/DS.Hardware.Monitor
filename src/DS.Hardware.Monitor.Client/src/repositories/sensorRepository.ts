import { Sensor } from "../model/sensor";
import { createRepository } from "../hooks/createRepository";

export const sensorRepository = createRepository<Sensor>();

export const useSensors = sensorRepository.useState;

export const useSensor = (id: number): Sensor | undefined =>
  sensorRepository.useState().filter((i) => i.id === id)[0];
