import { useEffect, useState } from "react"
import { Observable } from "rxjs"

/**
 * Returns a hook factory function for the given observable.
 */
export const hookFnFromObservable = <TObservable, TDefault>(obs: Observable<TObservable>, defaultValue: TDefault) => {
  return (): TObservable | TDefault => {
    const [v, set] = useState<TObservable | TDefault>(defaultValue)
    useEffect(() => {
      const sub = obs.subscribe(set)
      return () => sub.unsubscribe()
    }, [])
    return v
  }
}
