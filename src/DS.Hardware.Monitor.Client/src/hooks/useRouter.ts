import { matchPath, useLocation } from "react-router"
import { useNavigate } from "react-router-dom"

type Return = {
  /**
   * Navigates to the given URL.
   * @param replace
   * Replaces the current history item, instead of adding the URL to the history.
   */
  goto: (url: string, replace?: boolean) => void

  /**
   * Returns true if the given URL is active.
   */
  isActiveLink: (url: string) => boolean

  /**
   * Returns the current URL.
   */
  location: string
}

/**
 * Provides convenience methods to use router related methods.
 */
export const useRouter = (): Return => {
  const navigate = useNavigate()
  const location = useLocation()

  const isActiveLink = (url: string) => {
    return !!matchPath({ path: url, caseSensitive: true, end: true }, location.pathname)
  }

  const goto = (url: string, replace = false) => {
    if (replace) navigate(url, { replace })
    else if (!isActiveLink(url)) navigate(url)
  }

  return {
    goto,
    isActiveLink,
    location: location.pathname,
  }
}
