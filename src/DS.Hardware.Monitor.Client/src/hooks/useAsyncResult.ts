import { useEffect, useState } from "react"

export type State = "done" | "error" | "working";
type DoneResult<T> = { state: "done"; result: T; error: null }
type ErrorResult = { state: "error"; result: null; error: Error }
type WorkingResult<T> = { state: "working"; result: null | T; error: null }
export type AsyncResult<T> = WorkingResult<T> | ErrorResult | DoneResult<T>

type ReturnCallbacks<T> = { setValue: (result: T) => void; retrigger: () => void }

export type Return<T> = ReturnCallbacks<T> & (WorkingResult<T> | ErrorResult | DoneResult<T>)

/**
 * Asynchronously executes the given `taskFn` and also reevaluates the task if `param` changes.
 * Optional `setResult` allows to override the current result.
 * Intended use: loading async state after page visits.
 *
 *
 * @example
 * const fetchResult = useAsyncResult(param => fetchId(param), 5);
 * if (fetchResult.state == 'done')
 *     console.log(fetchResult.data)
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const useAsyncResult = <TParams extends any[], TResult>(
  taskFn: (...args: TParams) => Promise<TResult>,
  ...params: TParams
): Return<TResult> => {
  const [run, setRun] = useState(0)
  const [state, setState] = useState<AsyncResult<TResult>>({
    state: "working",
    result: null,
    error: null,
  })

  /* eslint-disable react-hooks/exhaustive-deps */
  useEffect(() => {
    setState(prev => ({
      state: "working",
      result: prev.result,
      error: null,
    }))

    let isCanceled = false

    taskFn(...params)
      .then(result => {
        if (isCanceled) return
        setState({
          state: "done",
          result,
          error: null,
        })
      })
      .catch(error => {
        console.error("useAsyncResult handler threw an exception", error)
        if (isCanceled) return
        setState({
          state: "error",
          result: null,
          error,
        })
      })
    return () => {
      isCanceled = true
    }
  }, [...params, run])
  /* eslint-enable react-hooks/exhaustive-deps */

  const updateResult = (result: TResult) => {
    if (state.state !== "done") throw new Error("Result can only be updated in 'done' state")

    setState({
      state: "done",
      result,
      error: null,
    })
  }

  return {
    ...state,
    setValue: updateResult,
    retrigger: () => setRun(i => i + 1),
  }
}
