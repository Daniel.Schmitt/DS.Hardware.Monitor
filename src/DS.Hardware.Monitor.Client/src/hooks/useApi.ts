import axios, { AxiosError, Method } from "axios";
import { useRouter } from "./useRouter";

const request = axios;

function isStatusCodeOk(statusCode: number): boolean {
    return statusCode >= 200 && statusCode < 300;
}

export const useApi = () => {
    const router = useRouter();

    const get = async <T>(relativeUrl: string, data?: any): Promise<T | null> => {
        return await executeRequest<T>("GET", data, relativeUrl);
    };

    const post = async <T>(relativeUrl: string, data: any): Promise<T | null> => {
        return await executeRequest<T>("POST", data, relativeUrl);
    };

    const put = async <T>(relativeUrl: string, data: any): Promise<T | null> => {
        return await executeRequest<T>("PUT", data, relativeUrl);
    };

    const deleteRequest = async <T>(relativeUrl: string, data?: any): Promise<T | null> => {
        return await executeRequest<T>("DELETE", data, relativeUrl);
    };

    const executeRequest = async <T>(
        method: Method,
        data: any,
        relativeUrl: string
    ): Promise<T | null> => {
        const url = `/api/${relativeUrl}`;
        try {
            const response = await request(url, { method: method, data: data })

            if (response.status === 204) return null;
            if (!isStatusCodeOk(response.status))
                throw new Error(`Request to ${url} failed with status code ${response.status}`);
            if (response.data === null) throw new Error(`Request to ${url} failed with no data`);
            return response.data;
        } catch (error) {
            throw error;
        }
    };
    
    return {
        get,
        delete: deleteRequest,
        post,
        put,
    };
};
