import { createProperty } from './createProperty'
import { List } from 'linqts'

/**
 * Creates a generic repository with add/update/delete functionality.
 */
export const createRepository = <T extends { id: number }>() => {
  const items = createProperty<T[]>([])

  const replace = (newValues: T[]) => {
    const ids = new List(newValues.map(i => i.id)).Distinct().ToArray()
    if (ids.length !== newValues.length) {
      throw new Error('Event ids must be unique')
    }
    items.set(newValues)
  }

  const update = (item: T) => {
    const currentItems = items.value()
    if (currentItems.findIndex(i => i.id === item.id) < 0) {
      throw new Error('Item with id not found: ' + item.id)
    }

    items.set([...currentItems.filter(i => i.id !== item.id), item])
  }

  const remove = (item: T) => {
    items.set(items.value().filter(i => i.id !== item.id))
  }

  const add = (item: T) => {
    const currentItems = items.value()
    if (currentItems.findIndex(i => i.id === item.id) >= 0) {
      throw new Error('Duplicate item id: ' + item.id)
    }

    items.set([...currentItems, item])
  }

  return { replace, update, remove, add, useState: items.useState, values: items.value }
}
