import { useMediaQuery, useTheme } from "@mui/material";

export const useScreenSize = () => {
  const theme = useTheme();

  const get = (): "desktop" | "mobile" => {
    return isDesktop() ? "desktop" : "mobile";
  };

  const isDesktop = () => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    return useMediaQuery(theme.breakpoints.up("lg"));
  };

  const isMobile = () => {
    return !isDesktop();
  };

  return {
    get,
    isDesktop,
    isMobile,
  };
};
