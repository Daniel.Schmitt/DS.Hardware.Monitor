import { cloneDeep, isEqual } from "lodash"
import { useCallback, useMemo } from "react"
import { BehaviorSubject, map, distinctUntilChanged, Observable } from "rxjs"
import { hookFnFromObservable } from "./hookFnFromObservable"

type Return<T> = {
  /**
   * Gets the current value.
   */
  value: () => T

  /**
   * Sets the current value.
   */
  set: (newValue: T) => void

  /**
   * Gets the underlying observable.
   */
  observable: Observable<T>

  /**
   * Gets a convenience hook to fetch the current value or a part of it.
   * Note: when using the selector, value updates are only fetched if the resulting value changed (using deep equal comparison).
   */
  useState: {
    (): T
    <TRet>(selectorFn?: (value: T) => TRet): TRet
  }

  /**
   * Updates the value either by partially overwriting it or by using a mutate function.
   */
  update: {
    (partialValue: Partial<T>): void
    (mutateFn: (value: T) => void): void
  }
}

/**
 * Creates an exposable property for a service.
 */
export const createProperty = <T>(defaultValue: T): Return<T> => {
  let value: T = defaultValue

  const subject = new BehaviorSubject<T>(defaultValue)

  function useState(): T
  function useState<TRet>(selectorFn?: (value: T) => TRet): TRet
  function useState<TRet>(selectorFn?: (value: T) => TRet) {
    const value = subject.getValue()
    if (!selectorFn) return hookFnFromObservable(subject, value)()
    const selector = useCallback(selectorFn, [])
    return useMemo(
      () =>
        hookFnFromObservable(
          subject.pipe(
            map(selector),
            distinctUntilChanged((p, q) => isEqual(p, q))
          ),
          selectorFn(value)
        ),
      []
    )()
  }

  function update(partialValue: Partial<T>): void
  function update(mutateFn: (value: T) => void): void
  function update(param: Partial<T> | ((value: T) => void)) {
    if (typeof param === "function") {
      const immediate = cloneDeep(value)
      param(immediate)
      set(immediate)
    } else {
      set({ ...value, ...param })
    }
  }

  const set = (newValue: T) => {
    value = newValue
    subject.next(newValue)
  }

  return {
    value: () => value,

    set,

    update,

    observable: subject.asObservable(),

    useState,
  }
}
