export type Sensor = {
  id: number;
  name: string;
  type: SensorType;
  value: number;
  unit: string;
  dateTime: Date;
};

export enum SensorType {
  Temperature = 0,
  Load = 1,
  Power = 2,
  Voltage = 3,
  SmallData = 4,
  Data = 5,
}
