import { List } from "linqts";

declare global {
  interface Array<T> {
    toList(): List<T>;
  }
}

Array.prototype.toList = function <T>(this: Array<T>) {
  return new List<T>(this);
};
