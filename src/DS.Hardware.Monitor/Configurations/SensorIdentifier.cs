namespace DS.Hardware.Monitor.Configurations
{
    public class Sensor
    {
        public string? Name { get; set; }
        public string Identifier { get; set; }
        public string? Unit { get; set; }
    }
}