namespace DS.Hardware.Monitor.Configurations
{
    public class ApplicationConfiguration
    {
        public TimeSpan ScanInterval { get; set; } = TimeSpan.FromMilliseconds(1000);

        public List<Sensor> Sensors { get; set; } = new List<Sensor>
        {
            new Sensor{ Identifier = "/intelcpu/0/temperature/14", Name = "CPU Temperature"},
            new Sensor{ Identifier = "/gpu-nvidia/0/temperature/0", Name = "GPU Temperature"}
        };
        public TimeSpan DeleteSensorsExecutionRate { get; set; } = TimeSpan.FromMinutes(5);
    }
}