using System.Reactive.Linq;
using AutoMapper;
using DS.Hardware.Monitor.Configurations;
using DS.Hardware.Monitor.Extensions;
using DS.Hardware.Monitor.Hubs;
using DS.Hardware.Monitor.Models;
using DS.Hardware.Monitor.Persistence;
using DS.Hardware.Monitor.Persistence.Model;
using EnumsNET;
using LibreHardwareMonitor.Hardware;
using Microsoft.AspNetCore.SignalR;

namespace DS.Hardware.Monitor.Services
{
    public class HardwareMonitorService : IHostedService
    {
        private readonly ApplicationConfiguration applicationConfiguration;
        private readonly IHubContext<SensorHub, ISensorHub> sensorHubContext;
        private readonly IMapper mapper;
        private readonly Context context;
        private IDisposable disposable;
        private readonly Computer computer;
        private readonly ILogger<HardwareMonitorService> logger;

        public HardwareMonitorService(
            ILogger<HardwareMonitorService> logger,
            ApplicationConfiguration applicationConfiguration,
            IHubContext<SensorHub, ISensorHub> sensorHubContext,
            IMapper mapper,
            IServiceScopeFactory serviceScopeFactory)
        {
            this.logger = logger;
            this.applicationConfiguration = applicationConfiguration;
            this.sensorHubContext = sensorHubContext;
            this.mapper = mapper;
            this.context = serviceScopeFactory.CreateScope().ServiceProvider.GetRequiredService<Context>();

            this.computer = new Computer
            {
                IsCpuEnabled = this.applicationConfiguration.Sensors.ShouldEnableCPU(),
                IsGpuEnabled = this.applicationConfiguration.Sensors.ShouldEnableGPU(),
                IsMemoryEnabled = this.applicationConfiguration.Sensors.ShouldEnableMemory(),
                IsPsuEnabled = this.applicationConfiguration.Sensors.ShouldEnablePSU(),
            };
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            this.disposable = Observable.Interval(this.applicationConfiguration.ScanInterval)
                                        .Subscribe(async _ => await this.UpdateSensorValues());

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            this.disposable.Dispose();

            return Task.CompletedTask;
        }

        private async Task UpdateSensorValues()
        {
            this.computer.Open();
            this.computer.Accept(new UpdateVisitor());

            var sensorDtos = new List<SensorDto>();

            foreach (var sensor in this.applicationConfiguration.Sensors)
            {
                var hardwareType = sensor.ResolveHardwareType();
                var sensorType = sensor.ResolveSensorType();

                sensorDtos.Add(this.CreateSensor(
                   hardwareType,
                   sensor.Identifier,
                   sensorType,
                   sensor.Name,
                   sensor.Unit
               ));
            }

            if (sensorDtos.Count > 0)
            {
                var sensors = this.mapper.Map<List<Persistence.Model.Sensor>>(sensorDtos);
                await this.context.AddRangeAsync(sensors);
                await this.context.SaveChangesAsync();

                // Update SensorDto ids
                foreach (var sensorDto in sensorDtos)
                    sensorDto.Id = sensors.Single(x => x.Name == sensorDto.Name).Id;

                await this.sensorHubContext.Clients.All.UpdateSensors(sensorDtos);
            }

            this.computer.Close();
        }

        private SensorDto CreateSensor(HardwareType hardwareType, string identifier, Persistence.Model.SensorType sensorType, string? name = null, string? unit = null)
        {
            var hardware = this.computer.Hardware.Single(x => x.HardwareType == hardwareType);
            var sensor = hardware.Sensors.Single(x => x.Identifier.ToString() == identifier);

            var newUnit = unit ?? sensorType.AsString(EnumFormat.Description)!;
            this.logger.LogDebug($"Sensor: {sensor.Name}, value: {sensor.Value} {newUnit}");

            return new SensorDto
            {
                Type = sensorType,
                Name = string.IsNullOrEmpty(name) ? sensor.Name : name,
                Value = sensor.Value ?? 0,
                Unit = newUnit
            };
        }
    }
}