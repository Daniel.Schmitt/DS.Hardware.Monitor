// <copyright file="TimedMediatRService.cs" company="Daniel Schmitt">Copyright (c) Daniel Schmitt. All rights reserved.</copyright>

using System.Reactive.Linq;
using MediatR;

namespace DS.Hardware.Monitor.Services
{
    public class TimedMediatRService : IHostedService
    {
        private readonly IServiceScopeFactory serviceScopeFactory;
        private readonly TimeSpan executionRate;
        private readonly IRequest<Unit> mediatRRequest;
        private IDisposable? timerSubscription;

        public TimedMediatRService(
            IServiceScopeFactory serviceScopeFactory,
            TimeSpan executionRate,
            IRequest<Unit> mediatRRequest)
        {
            this.serviceScopeFactory = serviceScopeFactory;
            this.executionRate = executionRate;
            this.mediatRRequest = mediatRRequest;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            this.timerSubscription = Observable.Interval(this.executionRate)
                .Subscribe(_ =>
                {
                    using var scope = this.serviceScopeFactory.CreateScope();
                    var mediatRWrapper = scope.ServiceProvider.GetService<IMediator>();
                    mediatRWrapper.Send(this.mediatRRequest);
                });

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            this.timerSubscription?.Dispose();
            return Task.CompletedTask;
        }
    }
}