﻿// <copyright file="DeactivateChallengesService.cs" company="Daniel Schmitt">Copyright (c) Daniel Schmitt. All rights reserved.</copyright>

using DS.Hardware.Monitor.Configurations;
using DS.Hardware.Monitor.MediatR.Sensors.Delete;

namespace DS.Hardware.Monitor.Services
{
    public class DeleteSensorsService : TimedMediatRService
    {
        public DeleteSensorsService(IServiceScopeFactory serviceScopeFactory, ApplicationConfiguration applicationConfiguration)
            : base(serviceScopeFactory, applicationConfiguration.DeleteSensorsExecutionRate, new DeleteSensorsCommand())
        {
        }
    }
}
