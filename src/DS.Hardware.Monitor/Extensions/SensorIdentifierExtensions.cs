using System.Linq;
using DS.Hardware.Monitor.Configurations;
using DS.Hardware.Monitor.Constants;
using LibreHardwareMonitor.Hardware;

namespace DS.Hardware.Monitor.Extensions
{
    public static class SensorIdentifierExtensions
    {
        public static HardwareType ResolveHardwareType(this Sensor sensor)
        {
            var hardwareTypeString = sensor.Identifier.Split('/')[1];

            return hardwareTypeString switch
            {
                HardwareTypeConstants.CPUIntel => HardwareType.Cpu,
                HardwareTypeConstants.CPUAmd => HardwareType.Cpu,
                HardwareTypeConstants.GPUNvidia => HardwareType.GpuNvidia,
                HardwareTypeConstants.GPUAmd => HardwareType.GpuAmd,
                HardwareTypeConstants.PSU => HardwareType.Psu,
                HardwareTypeConstants.RAM => HardwareType.Memory,
                _ => throw new NotSupportedException($"Identifier with hardware type {hardwareTypeString} is not supported"),
            };
        }

        public static Persistence.Model.SensorType ResolveSensorType(this Sensor sensor)
        {
            var index = sensor.Identifier.Count(x => x == '/') - 1;
            var sensorTypeString = sensor.Identifier.Split('/')[index];

            return sensorTypeString switch
            {
                SensorTypeConstants.Temperature => Persistence.Model.SensorType.Temperature,
                SensorTypeConstants.Load => Persistence.Model.SensorType.Load,
                SensorTypeConstants.Power => Persistence.Model.SensorType.Power,
                SensorTypeConstants.Voltage => Persistence.Model.SensorType.Voltage,
                SensorTypeConstants.SmallData => Persistence.Model.SensorType.SmallData,
                SensorTypeConstants.Data => Persistence.Model.SensorType.Data,
                _ => throw new NotSupportedException($"Identifier with sensor type {sensorTypeString} is not supported"),
            };
        }

        public static bool ShouldEnableCPU(this List<Sensor> sensors)
            => sensors.Exists(sensor => HardwareTypeConstants.CPUs.Any(x => sensor.Identifier.Contains(x)));

        public static bool ShouldEnableGPU(this List<Sensor> sensors)
            => sensors.Exists(sensor => HardwareTypeConstants.GPUs.Any(x => sensor.Identifier.Contains(x)));

        public static bool ShouldEnableMemory(this List<Sensor> sensors)
            => sensors.Exists(sensor => HardwareTypeConstants.Memories.Any(x => sensor.Identifier.Contains(x)));

        public static bool ShouldEnablePSU(this List<Sensor> sensors)
            => sensors.Exists(sensor => HardwareTypeConstants.PSUs.Any(x => sensor.Identifier.Contains(x)));
    }
}