using DS.Hardware.Monitor.Persistence.Model;

namespace DS.Hardware.Monitor.Models
{
    public class SensorDto
    {
        public SensorDto()
        {
            this.DateTime = DateTime.Now;
        }
        public int Id { get; set; }

        public string Name { get; set; }

        public SensorType Type { get; set; }

        public DateTime DateTime { get; set; }

        public string Unit { get; set; }

        public float Value { get; set; }
    }
}