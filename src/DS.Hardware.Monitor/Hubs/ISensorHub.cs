using DS.Hardware.Monitor.Models;

namespace DS.Hardware.Monitor.Hubs
{
    public interface ISensorHub
    {
        Task Initialize(List<SensorDto> sensors);

        Task UpdateSensors(List<SensorDto> sensors);
    }
}