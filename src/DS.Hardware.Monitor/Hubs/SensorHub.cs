using AutoMapper;
using DS.Hardware.Monitor.Models;
using DS.Hardware.Monitor.Persistence;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;

namespace DS.Hardware.Monitor.Hubs
{
    public class SensorHub : Hub<ISensorHub>
    {
        private readonly Context context;
        private readonly IMapper mapper;

        public SensorHub(IServiceScopeFactory serviceScopeFactory, IMapper mapper)
        {
            this.context = serviceScopeFactory.CreateScope().ServiceProvider.GetRequiredService<Context>();
            this.mapper = mapper;
        }

        public override async Task OnConnectedAsync()
        {
            var sensors = await this.context.Sensors.AsNoTracking().ToListAsync();
            await this.Clients.Caller.Initialize(this.mapper.Map<List<SensorDto>>(sensors));
        }
    }
}