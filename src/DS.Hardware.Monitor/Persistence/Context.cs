using DS.Hardware.Monitor.Persistence.Model;
using Microsoft.EntityFrameworkCore;

namespace DS.Hardware.Monitor.Persistence
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options)
          : base(options)
        {
        }

#nullable disable
        public DbSet<Sensor> Sensors { get; private set; }
#nullable restore


    }
}