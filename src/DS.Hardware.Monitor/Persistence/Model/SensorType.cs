using System.ComponentModel;

namespace DS.Hardware.Monitor.Persistence.Model
{
    public enum SensorType
    {
        [Description("°C")]
        Temperature,

        [Description("%")]
        Load,

        [Description("W")]
        Power,

        [Description("V")]
        Voltage,

        [Description("Mb")]
        SmallData,

        [Description("Gb")]
        Data,
    }
}