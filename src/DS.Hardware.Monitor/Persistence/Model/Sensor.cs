using System.ComponentModel.DataAnnotations;

namespace DS.Hardware.Monitor.Persistence.Model
{
    public class Sensor
    {
        public Sensor()
        {
            this.DateTime = DateTime.Now;
        }

        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public SensorType Type { get; set; }

        [Required]
        public DateTime DateTime { get; set; }

        [Required]
        public float Value { get; set; }
    }
}