using DS.Hardware.Monitor.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace DS.Hardware.Monitor.Persistence.Migrations
{
    public class MigrationContextFactory : IDesignTimeDbContextFactory<Context>
    {
        public Context CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<Context>();

            optionsBuilder.UseSqlite("Data Source=database.sqlite;");

            return new Context(optionsBuilder.Options);
        }
    }
}
