namespace DS.Hardware.Monitor.Constants
{
    public static class HardwareTypeConstants
    {
        // CPU
        public const string CPUIntel = "intelcpu";
        public const string CPUAmd = "amdcpu";
        public static string[] CPUs = { CPUIntel, CPUAmd };

        // GPU
        public const string GPUNvidia = "gpu-nvidia";
        public const string GPUAmd = "gpu-amd";
        public static string[] GPUs = { GPUNvidia, GPUAmd };

        // Memory
        public const string RAM = "ram";
        public static string[] Memories = { RAM };

        // PSU
        public const string PSU = "psu";
        public static string[] PSUs = { PSU };
    }
}