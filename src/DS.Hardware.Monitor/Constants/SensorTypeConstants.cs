namespace DS.Hardware.Monitor.Constants
{
    public static class SensorTypeConstants
    {
        public const string Temperature = "temperature";
        public const string Load = "load";
        public const string Power = "power";
        public const string Voltage = "voltage";
        public const string SmallData = "smalldata";
        public const string Data = "data";
    }
}