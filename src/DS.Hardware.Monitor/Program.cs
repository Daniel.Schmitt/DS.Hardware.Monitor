﻿using DS.Hardware.Monitor.Configurations;
using DS.Hardware.Monitor.Persistence;
using DS.Hardware.Monitor.Services;
using Microsoft.EntityFrameworkCore;
using Serilog;
using DS.Hardware.Monitor.Hubs;
using LibreHardwareMonitor.Hardware;
using DS.Hardware.Monitor.Mappings;
using Microsoft.Extensions.Hosting.WindowsServices;
using DS.Hardware.Monitor.MediatR.Sensors.Delete;
using MediatR;

namespace DS.Hardware.Monitor
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var options = new WebApplicationOptions
            {
                Args = args,
                ContentRootPath = System.AppDomain.CurrentDomain.BaseDirectory
            };

            var builder = WebApplication.CreateBuilder(options);

            builder.Configuration.SetBasePath(builder.Environment.ContentRootPath)
                                 .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                                 .AddJsonFile("appsettings.Development.json", optional: true, reloadOnChange: true);

            builder.Services.AddOptions();
            builder.Services.Configure<ApplicationConfiguration>(builder.Configuration.GetSection(nameof(ApplicationConfiguration)));
            builder.Services.AddTransient<ApplicationConfiguration>(x =>
            {
                var applicationConfiguration = builder.Configuration.GetSection(nameof(ApplicationConfiguration))?.Get<ApplicationConfiguration>();

                // This has to be done to ensure the list is in correct order
                if (applicationConfiguration != null)
                    applicationConfiguration.Sensors = x.GetRequiredService<IConfiguration>().GetSection($"{nameof(ApplicationConfiguration)}:{nameof(ApplicationConfiguration.Sensors)}").Get<List<Sensor>>();

                return applicationConfiguration ?? new ApplicationConfiguration();
            });
            builder.Services.AddSignalR();
            builder.Services.AddAutoMapper(typeof(SensorMappings).Assembly);

            builder.Services.AddDbContext<Context>((serviceProvider, options) =>
            {
                var configuration = serviceProvider.GetRequiredService<IConfiguration>();
                var connectionString = configuration.GetConnectionString("ConnectionString");

                options.UseSqlite(connectionString);
            });

            builder.Services.AddMediatR(typeof(DeleteSensorsCommand).Assembly);
            builder.Host.UseSerilog((context, services, configuration) => configuration.ReadFrom.Configuration(context.Configuration));
            builder.Host.UseWindowsService();

            builder.Services.AddHostedService<HardwareMonitorService>();
            builder.Services.AddHostedService<DeleteSensorsService>();

            var app = builder.Build();

            using var scope = app.Services.CreateScope();
            var logger = scope.ServiceProvider.GetRequiredService<ILogger<Program>>();

            logger.LogCritical(options.ContentRootPath);

            // Check if sensor names are unique
            var applicationConfiguration = scope.ServiceProvider.GetRequiredService<ApplicationConfiguration>();
            var sensorsWithSameName = applicationConfiguration.Sensors.Except(applicationConfiguration.Sensors.DistinctBy(x => x.Name));
            if (sensorsWithSameName.Count() > 0)
                throw new ApplicationException($"Sensor names has to be unique!{Environment.NewLine}Following sensor names are not unique: {string.Join(", ", sensorsWithSameName.Select(x => $"'{x.Name}'"))}");

            var context = scope.ServiceProvider.GetRequiredService<Context>();
            context.Database.Migrate();

            app.UseRouting();
            app.UseStaticFiles();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<SensorHub>("/hubs/sensorHub");
                endpoints.Map("/status", () => DateTime.Now);
            });

            app.MapFallbackToFile("index.html");

            PrintAllComputerInformation(logger);

            app.Run();
        }

        private static void PrintAllComputerInformation(ILogger<Program> logger)
        {
            Computer computer = new Computer
            {
                IsCpuEnabled = true,
                IsGpuEnabled = true,
                IsMemoryEnabled = true,
                IsMotherboardEnabled = true,
                IsControllerEnabled = true,
                IsNetworkEnabled = true,
                IsStorageEnabled = true
            };

            computer.Open();
            computer.Accept(new UpdateVisitor());

            foreach (IHardware hardware in computer.Hardware)
            {
                foreach (IHardware subhardware in hardware.SubHardware)
                {
                    foreach (ISensor sensor in subhardware.Sensors)
                    {
                        logger.LogDebug($"[Sub] Hardware: {hardware.Name}, Sensor: {sensor.Name}, identifier: {sensor.Identifier}, value: {sensor.Value}");
                    }
                }

                foreach (ISensor sensor in hardware.Sensors)
                {
                    logger.LogDebug($"Hardware: {hardware.Name}, Sensor: {sensor.Name}, identifier: {sensor.Identifier}, value: {sensor.Value}");
                }
            }

            computer.Close();
        }
    }
}