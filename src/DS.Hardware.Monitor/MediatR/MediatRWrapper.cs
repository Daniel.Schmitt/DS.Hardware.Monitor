// <copyright file="MediatRWrapper.cs" company="Daniel Schmitt">Copyright (c) Daniel Schmitt. All rights reserved.</copyright>

using System.Diagnostics;
using System.Security.Claims;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace DS.Hardware.Monitor.MediatR
{
    public class MediatRWrapper : IMediatRWrapper
    {
        private readonly IMediator mediator;
        private readonly ILogger<MediatRWrapper> logger;
        private readonly IHttpContextAccessor httpContextAccessor;

        public MediatRWrapper(IMediator mediator, ILogger<MediatRWrapper> logger, IHttpContextAccessor httpContextAccessor)
        {
            this.mediator = mediator;
            this.logger = logger;
            this.httpContextAccessor = httpContextAccessor;
        }

        public async Task<ActionResult<TResponse>> Send<TResponse>(IRequest<TResponse> request)
        {
            try
            {
                var updateActivityTask = Task.FromResult(Unit.Value);

                var stopwatch = Stopwatch.StartNew();

                var requestTask = this.mediator.Send(request);

                await Task.WhenAll(requestTask);

                stopwatch.Stop();

                this.logger.LogTrace($"{request.GetType().Name} took {stopwatch.ElapsedMilliseconds} ms");

                if (requestTask.Result != null
                    && EqualityComparer<TResponse>.Default.Equals(requestTask.Result, default)
                    && requestTask.Result.GetType() != typeof(int)
                    && requestTask.Result.GetType() != typeof(Unit)
                    && requestTask.Result.GetType() != typeof(bool))
                {
                    return new NotFoundResult();
                }

                return new OkObjectResult(requestTask.Result);
            }
            catch (CustomStatusCodeException exception)
            {
                this.logger.LogWarning(exception, exception.Message);

                return new ObjectResult(exception.Message) { StatusCode = (int)exception.HttpStatusCode };
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex, ex.Message);

                return new ObjectResult(ex.Message) { StatusCode = StatusCodes.Status500InternalServerError };
            }
        }
    }
}