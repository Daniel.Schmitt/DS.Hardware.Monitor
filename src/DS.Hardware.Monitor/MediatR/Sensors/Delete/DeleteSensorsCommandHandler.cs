﻿// <copyright file="DeleteChallengeDtoCommandHandler.cs" company="Daniel Schmitt">Copyright (c) Daniel Schmitt. All rights reserved.</copyright>

using System.Net;
using DS.Hardware.Monitor.Configurations;
using DS.Hardware.Monitor.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace DS.Hardware.Monitor.MediatR.Sensors.Delete
{
    public class DeleteSensorsCommandHandler : IRequestHandler<DeleteSensorsCommand, Unit>
    {
        private readonly ILogger<DeleteSensorsCommandHandler> logger;
        private readonly Context databaseContext;

        public DeleteSensorsCommandHandler(
            ILogger<DeleteSensorsCommandHandler> logger,
            Context databaseContext)
        {
            this.logger = logger;
            this.databaseContext = databaseContext;
        }

        public async Task<Unit> Handle(DeleteSensorsCommand request, CancellationToken cancellationToken)
        {
            this.logger.LogInformation($"Run {nameof(DeleteSensorsCommand)}");

            var oldSensors = await this.databaseContext.Sensors
                                            .Where(x => x.DateTime.AddHours(1) < DateTime.Now)
                                            .ToListAsync();

            this.databaseContext.Sensors.RemoveRange(oldSensors);

            await this.databaseContext.SaveChangesAsync();

            this.logger.LogInformation($"Deleted {oldSensors.Count} Sensors");

            return Unit.Value;
        }
    }
}
