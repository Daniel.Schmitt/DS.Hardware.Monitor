﻿// <copyright file="DeleteChallengeDtoCommand.cs" company="Daniel Schmitt">Copyright (c) Daniel Schmitt. All rights reserved.</copyright>

using DS.Hardware.Monitor.Persistence.Model;
using MediatR;

namespace DS.Hardware.Monitor.MediatR.Sensors.Delete
{
    public class DeleteSensorsCommand : IRequest<Unit>
    {
    }
}
