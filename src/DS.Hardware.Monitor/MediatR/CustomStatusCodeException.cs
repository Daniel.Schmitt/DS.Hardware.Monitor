// <copyright file="CustomStatusCodeException.cs" company="Daniel Schmitt">Copyright (c) Daniel Schmitt. All rights reserved.</copyright>

using System.Net;

namespace DS.Hardware.Monitor.MediatR
{
    public class CustomStatusCodeException : Exception
    {
        public CustomStatusCodeException(HttpStatusCode httpStatusCode, string message)
        : base(message)
        {
            this.HttpStatusCode = httpStatusCode;
        }

        public HttpStatusCode HttpStatusCode { get; set; }
    }
}