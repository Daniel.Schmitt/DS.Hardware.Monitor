// <copyright file="IMediatRWrapper.cs" company="Daniel Schmitt">Copyright (c) Daniel Schmitt. All rights reserved.</copyright>

using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DS.Hardware.Monitor.MediatR
{
    public interface IMediatRWrapper
    {
        Task<ActionResult<TResponse>> Send<TResponse>(IRequest<TResponse> request);
    }
}