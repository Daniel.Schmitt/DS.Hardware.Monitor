using AutoMapper;
using DS.Hardware.Monitor.Models;
using DS.Hardware.Monitor.Persistence.Model;
using EnumsNET;

namespace DS.Hardware.Monitor.Mappings
{
    public class SensorMappings : Profile
    {
        public SensorMappings()
        {
            // TODO
            this.CreateMap<Sensor, SensorDto>()
                .ForMember(x => x.Unit, x => x.MapFrom(sensor => sensor.Type.AsString(EnumFormat.Description)));

            this.CreateMap<SensorDto, Sensor>();
        }
    }
}